const merge = require('webpack-merge');
const path = require('path');
const common = require('./webpack.common.js');
const vars = require('./common-vars');

module.exports = merge(common, {
  mode: 'development',
  devtool: 'inline-source-map',
  devServer: {
    contentBase: path.resolve(__dirname, vars.distDir),
    hot: true,
    watchContentBase: true,
    port: 3000,
    open: false,
    // allow connection from other device in network
    disableHostCheck: true,
    host: '0.0.0.0',
  },
});

import { Dispatch } from 'react';
import { AppActionTypes } from '../context';
import { tryActivateBluetooth, trySearchAndConnect } from '../services/bluetooth';
import { ConnectionAction } from '../reducers/connection.reducer';

// eslint-disable-next-line import/prefer-default-export
export const activateBluetooth = async (dispatch: Dispatch<AppActionTypes>) => {
  try {
    await tryActivateBluetooth();
    dispatch({ type: ConnectionAction.UPDATE_BLUETOOTH_ON, payload: true });
    await trySearchAndConnect();
    dispatch({ type: ConnectionAction.UPDATE_CONNECTED, payload: true });
  } catch (error) {
    console.error(error);
    // Realistically, would throw error here to let component handle display it
    // Alternatively, would dispatch error to store
  }
};

import { Dispatch } from 'react';
import { ConnectionAction } from '../reducers/connection.reducer';
import { AppActionTypes } from '../context';
import { LockAction } from '../reducers/lock.reducer';
import { LockStatus } from '../types/app.types';
import { AuthAction } from '../reducers/auth.reducer';

// eslint-disable-next-line import/prefer-default-export
export const logout = async (dispatch: Dispatch<AppActionTypes>) => {
  try {
    // Mocked the reset for the prototype to enable clicking through the prototype in a cycle
    dispatch({ type: LockAction.UPDATE_LOCK_STATUS, payload: LockStatus.LOCKED });
    dispatch({ type: ConnectionAction.UPDATE_BLUETOOTH_ON, payload: false });
    dispatch({ type: ConnectionAction.UPDATE_CONNECTED, payload: false });
    dispatch({ type: AuthAction.LOGOUT });
  } catch (error) {
    console.error(error);
  }
};

import { Dispatch } from 'react';
import { AppActionTypes } from '../context';
import { LockAction } from '../reducers/lock.reducer';
import { LockStatus } from '../types/app.types';
import { tryOpenLock } from '../services/bluetooth';

// eslint-disable-next-line import/prefer-default-export
export const openLock = async (dispatch: Dispatch<AppActionTypes>) => {
  try {
    dispatch({ type: LockAction.UPDATE_LOCK_STATUS, payload: LockStatus.OPENING });
    await tryOpenLock();
    dispatch({ type: LockAction.UPDATE_LOCK_STATUS, payload: LockStatus.OPEN }); // simulate for prototype
  } catch (error) {
    console.error(error);
    // Realistically, would throw error here to let component handle and display it
    // Alternatively, would dispatch error to store
  }
};

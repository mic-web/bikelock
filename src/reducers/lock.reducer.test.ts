import reducer, { LockAction } from './lock.reducer';
import { LockStatus, LockState } from '../types/app.types';

describe('Reducer', () => {
  describe('updates lock status', () => {
    test('from locked to opening', () => {
      const initial: LockState = {
        status: LockStatus.LOCKED,
      };
      const state = reducer(initial, { type: LockAction.UPDATE_LOCK_STATUS, payload: LockStatus.OPENING });
      const expected: LockState = {
        status: LockStatus.OPENING,
      };
      expect(state).toEqual(expected);
    });
    test('from opening to open', () => {
      const initial: LockState = {
        status: LockStatus.OPENING,
      };
      const state = reducer(initial, { type: LockAction.UPDATE_LOCK_STATUS, payload: LockStatus.OPEN });
      const expected: LockState = {
        status: LockStatus.OPEN,
      };
      expect(state).toEqual(expected);
    });
    test('from open to locked', () => {
      const initial: LockState = {
        status: LockStatus.OPEN,
      };
      const state = reducer(initial, { type: LockAction.UPDATE_LOCK_STATUS, payload: LockStatus.LOCKED });
      const expected: LockState = {
        status: LockStatus.LOCKED,
      };
      expect(state).toEqual(expected);
    });
  });
});

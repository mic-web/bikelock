import { AuthState } from '../types/app.types';
import { AppActionTypes } from '../context';

export enum AuthAction {
  LOGIN = 'AUTH/LOGIN',
  LOGOUT = 'AUTH/LOGOUT',
}

export type AuthActionTypes =
  | { type: AuthAction.LOGIN; payload: Pick<AuthState, 'email'> }
  | { type: AuthAction.LOGOUT };

const reducer = (state: AuthState, action: AppActionTypes): AuthState => {
  switch (action.type) {
    case AuthAction.LOGIN:
      return {
        ...state,
        loggedIn: true,
        email: action.payload.email,
      };
    case AuthAction.LOGOUT:
      return {
        ...state,
        loggedIn: false,
      };
    default:
      return state;
  }
};

export default reducer;

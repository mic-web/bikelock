import reducer, { ConnectionAction } from './connection.reducer';
import { ConnectionState } from '../types/app.types';

describe('Reducer', () => {
  describe('updates connection status', () => {
    test('of bluetoothOn', () => {
      const initial: ConnectionState = { bluetoothOn: false, connected: false };
      const state = reducer(initial, { type: ConnectionAction.UPDATE_BLUETOOTH_ON, payload: true });
      const expected: ConnectionState = { bluetoothOn: true, connected: false };
      expect(state).toEqual(expected);
    });
    test('of connected', () => {
      const initial: ConnectionState = { bluetoothOn: false, connected: false };
      const state = reducer(initial, { type: ConnectionAction.UPDATE_CONNECTED, payload: true });
      const expected: ConnectionState = { bluetoothOn: false, connected: true };
      expect(state).toEqual(expected);
    });
  });
});

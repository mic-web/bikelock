import { LockState, LockStatus } from '../types/app.types';
import { AppActionTypes } from '../context';

export enum LockAction {
  UPDATE_LOCK_STATUS = 'LOCK/UPDATE_LOCK_STATUS',
}

export type LockActionTypes = { type: LockAction.UPDATE_LOCK_STATUS; payload: LockStatus };

const reducer = (state: LockState, action: AppActionTypes): LockState => {
  switch (action.type) {
    case LockAction.UPDATE_LOCK_STATUS:
      return {
        ...state,
        status: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;

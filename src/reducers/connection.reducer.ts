import { ConnectionState } from '../types/app.types';
import { AppActionTypes } from '../context';

export enum ConnectionAction {
  UPDATE_BLUETOOTH_ON = 'CONNECTION/UPDATE_BLUETOOTH_ON',
  UPDATE_CONNECTED = 'CONNECTION/UPDATE_CONNECTED',
}

export type ConnectionActionTypes =
  | { type: ConnectionAction.UPDATE_BLUETOOTH_ON; payload: boolean }
  | { type: ConnectionAction.UPDATE_CONNECTED; payload: boolean };

const reducer = (state: ConnectionState, action: AppActionTypes): ConnectionState => {
  switch (action.type) {
    case ConnectionAction.UPDATE_BLUETOOTH_ON:
      return {
        ...state,
        bluetoothOn: action.payload,
      };
    case ConnectionAction.UPDATE_CONNECTED:
      return {
        ...state,
        connected: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;

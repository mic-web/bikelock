import reducer, { AuthAction } from './auth.reducer';
import { AuthState } from '../types/app.types';

describe('Reducer', () => {
  test('accepts email and logged in status', () => {
    const initial: AuthState = { email: null, loggedIn: false };
    const state = reducer(initial, {
      type: AuthAction.LOGIN,
      payload: { email: 'Mr. Foo' },
    });
    const expected: AuthState = { loggedIn: true, email: 'Mr. Foo' };
    expect(state).toEqual(expected);
  });
  test('accepts logout, does not forget email', () => {
    const initial: AuthState = { email: 'Mr. Foo', loggedIn: true };
    const state = reducer(initial, {
      type: AuthAction.LOGOUT,
    });
    const expected: AuthState = { loggedIn: false, email: 'Mr. Foo' };
    expect(state).toEqual(expected);
  });
});

import React from 'react';
import { Box, SvgIcon, Typography, Snackbar, makeStyles, fade } from '@material-ui/core';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import InfoIcon from '@material-ui/icons/Info';
import { LockStatus } from '../types/app.types';
import { useAppContext } from '../context';

type HintProps = {
  open: boolean;
  handleClose: () => void;
  message?: string;
  autoHideDuration: number;
};

const useStyles = makeStyles(({ palette }) => ({
  root: {
    display: 'flex',
    backgroundColor: fade(palette.background.paper, 0.9),
    color: palette.primary.dark,
    padding: '10px 15px 10px 15px',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  anchorOriginBottomCenter: {
    bottom: 0,
  },
}));

const Hint: React.FC<HintProps> = ({ open, handleClose, message, children, autoHideDuration }) => {
  const styles = useStyles();
  return (
    <Snackbar
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'center',
      }}
      open={open}
      autoHideDuration={autoHideDuration}
      onClose={handleClose}
      message={message}
      className={styles.root}
      classes={{
        anchorOriginBottomCenter: styles.anchorOriginBottomCenter,
      }}
    >
      <>{children}</>
    </Snackbar>
  );
};

enum ActiveToast {
  NONE = 'NONE',
  CONNECTED = 'CONNECTED',
  OPENED = 'OPENED',
}

const Toasts: React.FC = () => {
  const { state } = useAppContext();
  const [activeToast, setActiveToast] = React.useState<ActiveToast>(ActiveToast.NONE);
  React.useEffect(() => {
    if (state.connection.bluetoothOn && state.connection.connected) {
      setActiveToast(ActiveToast.CONNECTED);
    }
  }, [state.connection.bluetoothOn, state.connection.connected]);
  React.useEffect(() => {
    if (state.lock.status === LockStatus.OPEN) {
      setActiveToast(ActiveToast.OPENED);
    }
  }, [state.lock.status]);
  const close = React.useCallback(
    (toast: ActiveToast) => {
      if (toast === activeToast) {
        // Ensure that other toasts are not closed
        setActiveToast(ActiveToast.NONE);
      }
    },
    [activeToast]
  );
  return {
    [ActiveToast.CONNECTED]: (
      <Box display="flex" justifyContent="center">
        <Hint autoHideDuration={2000} handleClose={() => close(ActiveToast.CONNECTED)} open>
          <SvgIcon color="secondary">
            <CheckCircleIcon />
          </SvgIcon>
          <Box ml={1}>
            <Typography>Verbunden mit deinem AirLock</Typography>
          </Box>
        </Hint>
      </Box>
    ),
    [ActiveToast.OPENED]: (
      <Box display="flex" justifyContent="center">
        <Hint autoHideDuration={10000} handleClose={() => close(ActiveToast.OPENED)} open>
          <Box ml={1} display="flex" flexDirection="column" alignItems="flex-start">
            <Box display="flex" alignItems="center" justifyContent="flex-start">
              <SvgIcon>
                <InfoIcon />
              </SvgIcon>
              <Box ml={1}>
                <Typography>Verschließe manuell</Typography>
              </Box>
            </Box>
          </Box>
        </Hint>
      </Box>
    ),
    [ActiveToast.NONE]: null,
  }[activeToast];
};

export default Toasts;

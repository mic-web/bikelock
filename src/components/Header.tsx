import { AppBar, Toolbar, makeStyles, Box } from '@material-ui/core';
import React from 'react';

import Fade from '@material-ui/core/Fade';
import Menu from './Menu';

import Logo from '../img/Logo.icon';

const useStyles = makeStyles(({ palette }) => ({
  root: {
    color: palette.primary.contrastText,
  },
  logo: {
    width: 140,
    '--primary': palette.primary.contrastText,
    '--secondary': palette.primary.main,
  },
  colorPrimary: {
    backgroundColor: palette.primary.main,
  },
}));

export default function ButtonAppBar() {
  const styles = useStyles();
  return (
    <Fade in>
      <AppBar
        position="static"
        classes={{
          colorPrimary: styles.colorPrimary,
        }}
        className={styles.root}
      >
        <Toolbar variant="dense">
          <Logo className={styles.logo} />
          <Box ml="auto">
            <Menu />
          </Box>
        </Toolbar>
      </AppBar>
    </Fade>
  );
}

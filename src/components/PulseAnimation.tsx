import React from 'react';

import 'loaders.css';
import { Box, makeStyles, fade } from '@material-ui/core';

const useStyles = makeStyles(({ palette }) => ({
  loader: {
    transform: 'translate(50%, 50%)', // center animation within its container
    flex: 1,
    '& .ball-scale-multiple > div': {
      backgroundColor: fade(palette.common.white, 0.9),
    },
  },
}));

export default function PulseAnimation() {
  const styles = useStyles();
  return (
    <Box display="flex" width="40px" height="40px">
      <div className={styles.loader}>
        <div className="ball-scale-multiple">
          <div />
          <div />
          <div />
        </div>
      </div>
    </Box>
  );
}

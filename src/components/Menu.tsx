import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import MenuIcon from '@material-ui/icons/Menu';
import LiveHelpIcon from '@material-ui/icons/LiveHelp';
import CancelIcon from '@material-ui/icons/Cancel';
import WebIcon from '@material-ui/icons/Web';
import { Divider, IconButton, Box, SvgIcon } from '@material-ui/core';
import { useAppContext } from '../context';
import { logout } from '../actions/auth.actions';

const useStyles = makeStyles(({ palette }) => ({
  list: {
    width: 250,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    color: palette.primary.main,
  },
  openButton: {
    padding: 0,
  },
}));

export default function TemporaryDrawer() {
  const styles = useStyles();
  const [open, setOpen] = React.useState(false);
  const { dispatch } = useAppContext();

  const toggleDrawer = (nextOpen: boolean) => (event: React.KeyboardEvent | React.MouseEvent) => {
    if (
      event.type === 'keydown' &&
      ((event as React.KeyboardEvent).key === 'Tab' || (event as React.KeyboardEvent).key === 'Shift')
    ) {
      return;
    }
    setOpen(nextOpen);
  };

  const onLogout = () => logout(dispatch);

  return (
    <div>
      <>
        <IconButton onClick={toggleDrawer(true)} color="inherit" className={styles.openButton}>
          <MenuIcon />
        </IconButton>
        <Drawer anchor="right" open={open} onClose={toggleDrawer(false)}>
          <div
            className={styles.list}
            role="presentation"
            onClick={toggleDrawer(false)}
            onKeyDown={toggleDrawer(false)}
          >
            <List>
              <ListItem button onClick={toggleDrawer(false)}>
                <ListItemIcon>
                  <SvgIcon color="primary">
                    <CancelIcon />
                  </SvgIcon>
                </ListItemIcon>
                <ListItemText primary="Menü schließen" />
              </ListItem>
            </List>
            <Divider />
            <List>
              <ListItem button>
                <ListItemIcon>
                  <SvgIcon color="primary">
                    <LiveHelpIcon />
                  </SvgIcon>
                </ListItemIcon>
                <ListItemText primary="FAQ" />
              </ListItem>
              <ListItem button>
                <ListItemIcon>
                  <SvgIcon color="primary">
                    <WebIcon />
                  </SvgIcon>
                </ListItemIcon>
                <ListItemText primary="Produktwebsite" />
              </ListItem>
            </List>
            <Box mt="auto">
              <Divider />
              <List>
                <ListItem button onClick={onLogout}>
                  <ListItemIcon>
                    <SvgIcon color="primary">
                      <ExitToAppIcon />
                    </SvgIcon>
                  </ListItemIcon>
                  <ListItemText primary="Abmelden" />
                </ListItem>
              </List>
            </Box>
          </div>
        </Drawer>
      </>
    </div>
  );
}

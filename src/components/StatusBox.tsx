import { Box, Typography, makeStyles } from '@material-ui/core';
import React from 'react';

const useStyles = makeStyles(({ palette }) => ({
  textContainer: {
    background: palette.background.paper,
    padding: '10px 20px 10px 20px',
    borderRadius: 10,
    flex: 'none',
  },
  message: {
    color: palette.text.primary,
  },
}));
const StatusBox: React.FC<{ message: string; className?: string }> = ({ message, children }) => {
  const styles = useStyles();
  return (
    <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" flex="none">
      <Box
        display="flex"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        className={styles.textContainer}
      >
        {message && (
          <Typography variant="subtitle1" className={styles.message}>
            {message}
          </Typography>
        )}
        {children}
      </Box>
    </Box>
  );
};

export default StatusBox;

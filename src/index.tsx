import React from 'react';
import ReactDOM from 'react-dom';

import { AppWithProviders } from './App';
import './index.css';

const renderRootElement = (Element: React.FC) => {
  ReactDOM.render(<Element />, document.getElementById('root'));
};

renderRootElement(AppWithProviders);

if (module.hot) {
  module.hot.accept('./App', () => {
    // eslint-disable-next-line global-require
    renderRootElement(require('./App').default);
  });
}

import React, { createContext, Dispatch, useReducer } from 'react';
import { AppState, LockStatus } from './types/app.types';
import connectionReducer, { ConnectionActionTypes } from './reducers/connection.reducer';
import lockReducer, { LockActionTypes } from './reducers/lock.reducer';
import authReducer, { AuthActionTypes } from './reducers/auth.reducer';

export const initialState: AppState = {
  connection: {
    bluetoothOn: false,
    connected: false,
  },
  lock: {
    status: LockStatus.LOCKED,
  },
  auth: {
    loggedIn: false,
    email: null,
  },
};

export type AppActionTypes = ConnectionActionTypes | LockActionTypes | AuthActionTypes;

const AppContext = createContext<{
  state: AppState;
  dispatch: Dispatch<AppActionTypes>;
}>({
  state: initialState,
  dispatch: () => null,
});

const mainReducer = ({ connection, lock, auth }: AppState, action: AppActionTypes) => ({
  connection: connectionReducer(connection, action),
  lock: lockReducer(lock, action),
  auth: authReducer(auth, action),
});

const AppProvider: React.FC<{ state?: AppState }> = ({ children, state = initialState }) => {
  const [currentState, dispatch] = useReducer(mainReducer, state);

  return <AppContext.Provider value={{ state: currentState, dispatch }}>{children}</AppContext.Provider>;
};

const useAppContext = () => {
  const context = React.useContext(AppContext);
  if (!context) {
    throw new Error(`App context cannot be used outside of AppProvider`);
  }
  return context;
};

export { AppProvider, AppContext, useAppContext };

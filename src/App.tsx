import React from 'react';

import { makeStyles, Box } from '@material-ui/core';

import { AppThemeProvider } from './style/theme';
import Header from './components/Header';
import { AppProvider, useAppContext } from './context';
import Lock from './pages/Lock';
import Login from './pages/Login';
import Toasts from './components/Toasts';

const useStyles = makeStyles(({ palette }) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    flexGrow: 1,
    overflow: 'hidden',
    color: palette.primary.dark,
  },
}));

const App: React.SFC = () => {
  const { state } = useAppContext();
  const styles = useStyles();

  return (
    <div className={styles.root}>
      {!state.auth.loggedIn ? (
        <Box display="flex" flexDirection="column" alignItems="center" justifyContent="center" flex="1">
          <Login />
        </Box>
      ) : (
        <>
          <Header />
          <Box
            display="flex"
            flexDirection="column"
            alignItems="center"
            justifyContent="center"
            flex="1"
            overflow="hidden"
          >
            <Lock />
          </Box>
        </>
      )}
      <Toasts />
    </div>
  );
};

const AppWithProviders: React.FC = () => (
  <AppProvider>
    <AppThemeProvider>
      <App />
    </AppThemeProvider>
  </AppProvider>
);

export { AppWithProviders, App };

export type ConnectionState = {
  bluetoothOn: boolean;
  connected: boolean;
};

export enum LockStatus {
  LOCKED,
  OPEN,
  OPENING,
}

export type LockState = {
  status: LockStatus;
};

export type AuthState = {
  email: string | null;
  loggedIn: boolean;
};

export type AppState = {
  connection: ConnectionState;
  lock: LockState;
  auth: AuthState;
};

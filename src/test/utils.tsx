/* eslint-disable import/no-extraneous-dependencies */
// test-utils.js
import React from 'react';
import { render, RenderOptions } from '@testing-library/react';
import { AppThemeProvider } from '../style/theme';
import { AppProvider } from '../context';
import { AppState } from '../types/app.types';

const AllTheProviders: React.FC<{ state?: AppState }> = ({ children, state }) => {
  return (
    <AppThemeProvider>
      <AppProvider state={state}>{children}</AppProvider>
    </AppThemeProvider>
  );
};

type CustomRenderProps = { providerProps: { state: AppState }; renderOptions?: RenderOptions };
const customRender = (component: React.ReactElement, { providerProps, renderOptions }: CustomRenderProps) =>
  render(<AllTheProviders {...providerProps}>{component}</AllTheProviders>, renderOptions);

// re-export everything
export * from '@testing-library/react';
export { default as userEvent } from '@testing-library/user-event';

// override render method
export { customRender as render, AllTheProviders };

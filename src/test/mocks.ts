import { initialState } from '../context';
import { AppState } from '../types/app.types';

// eslint-disable-next-line import/prefer-default-export
export const getInitialMockState = (): AppState => ({
  ...initialState,
  auth: {
    loggedIn: true,
    email: 'mrfoo@gmail.com',
  },
});

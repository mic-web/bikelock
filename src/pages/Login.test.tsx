import React from 'react';
import { render, screen, userEvent, act, Screen, fireEvent } from '../test/utils';

import { App } from '../App';
import { AppState } from '../types/app.types';
import * as auth from '../services/auth';
import { getInitialMockState } from '../test/mocks';

const renderAuth = (state: AppState) => render(<App />, { providerProps: { state } });

describe('Login page', () => {
  const getLoginButton = (scr: Screen) => scr.getByRole('button');
  const getEmailInput = (scr: Screen) => scr.getByLabelText('email').querySelector('input');
  const getPasswordInput = (scr: Screen) => scr.getByLabelText('password').querySelector('input');

  test('renders an email and password field and a login button', () => {
    renderAuth({
      ...getInitialMockState(),
      auth: {
        loggedIn: false,
        email: null,
      },
    });

    expect(getLoginButton(screen)).toContainHTML('Anmelden');
    expect(getEmailInput(screen)).toBeInTheDocument();
    expect(getPasswordInput(screen)).toBeInTheDocument();
  });

  test('tries login and unlocks main page with header on success', async () => {
    renderAuth({
      ...getInitialMockState(),
      auth: {
        loggedIn: false,
        email: null,
      },
    });

    jest.spyOn(auth, 'tryLogin').mockImplementation(() => new Promise((resolve) => setTimeout(resolve, 0)));

    expect(screen.queryByRole('banner')).not.toBeInTheDocument();
    fireEvent.change(getEmailInput(screen), { target: { value: 'mrfoo@gmail.com' } });
    fireEvent.change(getPasswordInput(screen), { target: { value: '1234' } });
    await act(async () => userEvent.click(getLoginButton(screen)));
    expect(await screen.findByRole('banner')).toBeInTheDocument();

    jest.restoreAllMocks();
  });

  test('shows validation errors when entering insufficient input', async () => {
    renderAuth({
      ...getInitialMockState(),
      auth: {
        loggedIn: false,
        email: null,
      },
    });

    fireEvent.change(getEmailInput(screen), { target: { value: 'mrfoo' } });
    fireEvent.blur(getEmailInput(screen));
    expect(await screen.findByText('Ungültige Email-Adresse')).toBeInTheDocument();
    expect(getLoginButton(screen)).toBeDisabled();

    fireEvent.change(getEmailInput(screen), { target: { value: '' } });
    fireEvent.blur(getEmailInput(screen));
    expect(await screen.findByText('Email-Adresse ist erforderlich')).toBeInTheDocument();
    expect(getLoginButton(screen)).toBeDisabled();

    fireEvent.change(getPasswordInput(screen), { target: { value: '123' } });
    fireEvent.blur(getPasswordInput(screen));
    expect(await screen.findByText('Mindestens 4 Zeichen erforderlich')).toBeInTheDocument();
    expect(getLoginButton(screen)).toBeDisabled();
  });
});

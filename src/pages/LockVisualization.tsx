import React from 'react';
import { Box, useMediaQuery } from '@material-ui/core';
import PulseAnimation from '../components/PulseAnimation';

type Variant = 'subtle' | 'locked' | 'open' | 'opening';

const IconWrapper: React.FC<{ variant: Variant; loading?: boolean }> = ({ variant, loading }) => {
  const isSmall = useMediaQuery('(max-height:  400px)', { noSsr: true });
  const height = isSmall ? '150px' : '200px';

  const Icon: React.FC = () =>
    ({
      subtle: <Subtle />,
      locked: <Locked />,
      opening: <Opening />,
      open: <Open />,
    }[variant]);

  return (
    <Box display="flex" flexShrink={0} justifyContent="center" pt={1} position="relative">
      <svg height={height} viewBox="0 0 216 268" fill="none" xmlns="http://www.w3.org/2000/svg">
        <Icon />
      </svg>
      {loading && (
        <Box position="absolute" top="55%">
          <PulseAnimation />
        </Box>
      )}
    </Box>
  );
};

const Locked: React.FC = () => (
  <>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M204 109.538H203.999V109.165C204 109.289 204 109.414 204 109.538ZM203.991 108.027C203.279 48.2275 160.35 0 107.498 0C54.6492 0 11.7229 48.2204 11.0044 108.014C11.104 107.003 11.959 106.213 12.9966 106.202C18.108 106.153 23.1622 105.912 28.1483 105.49C29.9475 55.9688 64.5938 16.4719 107.07 16.4719C149.527 16.4719 184.161 55.9324 185.99 105.421C191.253 105.887 196.593 106.15 201.998 106.203C203.04 106.213 203.898 107.01 203.991 108.027Z"
      fill="url(#paint0_linear)"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M189.393 107.845H189.393V107.547C189.393 107.646 189.393 107.746 189.393 107.845ZM189.389 106.899C188.942 55.9955 152.45 14.887 107.498 14.887C62.5511 14.887 26.0612 55.9875 25.6072 106.884C25.6782 105.844 26.5458 105.023 27.6039 105.011C31.8382 104.962 36.0259 104.759 40.1586 104.409C41.6855 62.384 71.0875 28.8656 107.134 28.8656C143.165 28.8656 172.556 62.3531 174.108 104.351C178.477 104.737 182.908 104.96 187.392 105.011C188.455 105.023 189.326 105.852 189.389 106.899Z"
      fill="url(#paint1_linear)"
    />
    <g filter="url(#filter0_d)">
      <path
        d="M108 87.2424C74.4092 87.2453 41.2124 93.3463 8.53679 105.096C7.16499 105.589 6.01941 106.317 5.22096 107.204C4.42251 108.091 4.0008 109.104 4.00035 110.137C4.00035 147.413 3.75369 173.799 16.4417 196.696C29.1298 219.594 54.89 237.941 103.345 258.345C104.736 258.931 106.351 259.242 108 259.242C109.649 259.242 111.264 258.931 112.655 258.345C161.11 237.941 186.87 219.594 199.558 196.696C212.246 173.799 211.999 147.413 211.999 110.137C211.999 109.104 211.577 108.091 210.779 107.204C209.98 106.317 208.835 105.589 207.463 105.096C174.788 93.3468 141.593 87.2396 108 87.2424Z"
        fill="url(#paint2_radial)"
      />
    </g>
    <defs>
      <filter
        id="filter0_d"
        x="0"
        y="87.2424"
        width="216"
        height="180"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <linearGradient
        id="paint0_linear"
        x1="108.023"
        y1="-11.4755"
        x2="108.023"
        y2="123.1"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#DFDFDF" />
        <stop offset="1" stopColor="#959595" />
        <stop offset="1" stopColor="#A2A2A2" />
      </linearGradient>
      <linearGradient
        id="paint1_linear"
        x1="156.532"
        y1="110.06"
        x2="64.2067"
        y2="9.38947"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#C7C5C5" />
        <stop offset="1" stopColor="#959595" />
        <stop offset="1" stopColor="#5A5A5A" />
      </linearGradient>
      <radialGradient
        id="paint2_radial"
        cx="0"
        cy="0"
        r="1"
        gradientUnits="userSpaceOnUse"
        gradientTransform="translate(35 108) rotate(54.7618) scale(200.181 242.079)"
      >
        <stop stopColor="#4DA7E8" />
        <stop offset="0.552083" stopColor="#2F74A6" />
        <stop offset="1" stopColor="#0D3C5E" />
      </radialGradient>
    </defs>
  </>
);

const Open: React.FC = () => (
  <>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M65.0186 30.7397C77.1957 21.7011 91.6145 16.4719 107.07 16.4719C149.527 16.4719 184.161 55.9324 185.99 105.421C191.253 105.887 196.593 106.15 201.997 106.203C203.04 106.213 203.897 107.01 203.991 108.027C203.279 48.2275 160.35 0 107.498 0C88.2673 0 70.3507 6.38465 55.3035 17.3883L65.0186 30.7397ZM203.999 109.538H204C204 109.456 204 109.373 204 109.291L203.999 109.165V109.538Z"
      fill="url(#paint0_linear)"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M72.1115 40.4877C82.304 33.1161 94.2979 28.8656 107.134 28.8656C143.165 28.8656 172.556 62.3531 174.108 104.351C178.477 104.737 182.908 104.96 187.392 105.011C188.455 105.023 189.326 105.852 189.389 106.899C188.942 55.9955 152.45 14.887 107.498 14.887C91.4613 14.887 76.5011 20.1192 63.8701 29.1616L72.1115 40.4877ZM189.393 107.845H189.393C189.393 107.789 189.393 107.733 189.393 107.677L189.393 107.547V107.845Z"
      fill="url(#paint1_linear)"
    />
    <g filter="url(#filter0_d)">
      <path
        d="M108 87.2424C74.4091 87.2453 41.2123 93.3463 8.53666 105.096C7.16487 105.589 6.01929 106.317 5.22084 107.204C4.42239 108.091 4.00068 109.104 4.00022 110.137C4.00022 147.413 3.75357 173.799 16.4416 196.696C29.1296 219.594 54.8899 237.941 103.345 258.345C104.736 258.931 106.351 259.242 108 259.242C109.649 259.242 111.264 258.931 112.655 258.345C161.11 237.941 186.87 219.594 199.558 196.696C212.246 173.799 211.999 147.413 211.999 110.137C211.999 109.104 211.577 108.091 210.779 107.204C209.98 106.317 208.835 105.589 207.463 105.096C174.788 93.3468 141.593 87.2396 108 87.2424Z"
        fill="url(#paint2_radial)"
      />
    </g>
    <defs>
      <filter
        id="filter0_d"
        x="-0.00012207"
        y="87.2424"
        width="216"
        height="180"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <linearGradient
        id="paint0_linear"
        x1="108.022"
        y1="-11.4755"
        x2="108.022"
        y2="123.1"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#DFDFDF" />
        <stop offset="1" stopColor="#959595" />
        <stop offset="1" stopColor="#A2A2A2" />
      </linearGradient>
      <linearGradient
        id="paint1_linear"
        x1="156.532"
        y1="110.06"
        x2="64.2067"
        y2="9.38946"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#C7C5C5" />
        <stop offset="1" stopColor="#959595" />
        <stop offset="1" stopColor="#5A5A5A" />
      </linearGradient>
      <radialGradient
        id="paint2_radial"
        cx="0"
        cy="0"
        r="1"
        gradientUnits="userSpaceOnUse"
        gradientTransform="translate(34.9999 108) rotate(54.7618) scale(200.181 242.079)"
      >
        <stop stopColor="#82D3CF" />
        <stop offset="0.552083" stopColor="#399F93" />
        <stop offset="1" stopColor="#1C676A" />
      </radialGradient>
    </defs>
  </>
);

const Opening: React.FC = () => (
  <>
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M204 109.538H203.999V109.165C204 109.289 204 109.414 204 109.538ZM203.991 108.027C203.279 48.2275 160.35 0 107.498 0C54.6492 0 11.7229 48.2204 11.0044 108.014C11.104 107.003 11.959 106.213 12.9966 106.202C18.108 106.153 23.1622 105.912 28.1483 105.49C29.9475 55.9688 64.5938 16.4719 107.07 16.4719C149.527 16.4719 184.161 55.9324 185.99 105.421C191.253 105.887 196.593 106.15 201.998 106.203C203.04 106.213 203.898 107.01 203.991 108.027Z"
      fill="url(#paint0_linear)"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M189.393 107.845H189.393V107.547C189.393 107.646 189.393 107.746 189.393 107.845ZM189.389 106.899C188.942 55.9955 152.45 14.887 107.498 14.887C62.5511 14.887 26.0612 55.9875 25.6072 106.884C25.6782 105.844 26.5458 105.023 27.6039 105.011C31.8382 104.962 36.0259 104.759 40.1586 104.409C41.6855 62.384 71.0875 28.8656 107.134 28.8656C143.165 28.8656 172.556 62.3531 174.108 104.351C178.477 104.737 182.908 104.96 187.392 105.011C188.455 105.023 189.326 105.852 189.389 106.899Z"
      fill="url(#paint1_linear)"
    />
    <g filter="url(#filter0_d)">
      <path
        d="M108 87.2424C74.4092 87.2453 41.2124 93.3463 8.53679 105.096C7.16499 105.589 6.01941 106.317 5.22096 107.204C4.42251 108.091 4.0008 109.104 4.00035 110.137C4.00035 147.413 3.75369 173.799 16.4417 196.696C29.1298 219.594 54.89 237.941 103.345 258.345C104.736 258.931 106.351 259.242 108 259.242C109.649 259.242 111.264 258.931 112.655 258.345C161.11 237.941 186.87 219.594 199.558 196.696C212.246 173.799 211.999 147.413 211.999 110.137C211.999 109.104 211.577 108.091 210.779 107.204C209.98 106.317 208.835 105.589 207.463 105.096C174.788 93.3468 141.593 87.2396 108 87.2424Z"
        fill="url(#paint2_radial)"
      />
    </g>
    <defs>
      <filter
        id="filter0_d"
        x="0"
        y="87.2424"
        width="216"
        height="180"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <linearGradient
        id="paint0_linear"
        x1="108.023"
        y1="-11.4755"
        x2="108.023"
        y2="123.1"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#DFDFDF" />
        <stop offset="1" stopColor="#959595" />
        <stop offset="1" stopColor="#A2A2A2" />
      </linearGradient>
      <linearGradient
        id="paint1_linear"
        x1="156.532"
        y1="110.06"
        x2="64.2067"
        y2="9.38947"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#C7C5C5" />
        <stop offset="1" stopColor="#959595" />
        <stop offset="1" stopColor="#5A5A5A" />
      </linearGradient>
      <radialGradient
        id="paint2_radial"
        cx="0"
        cy="0"
        r="1"
        gradientUnits="userSpaceOnUse"
        gradientTransform="translate(35 108) rotate(54.7618) scale(200.181 242.079)"
      >
        <stop stopColor="#B596D6" />
        <stop offset="0.552083" stopColor="#753BBD" />
        <stop offset="1" stopColor="#1E0058" />
      </radialGradient>
    </defs>
  </>
);

const Subtle: React.FC = () => (
  <g opacity="0.5">
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M204 109.538H203.999V109.165C204 109.289 204 109.414 204 109.538ZM203.991 108.027C203.279 48.2275 160.35 0 107.498 0C54.6492 0 11.7229 48.2204 11.0044 108.014C11.104 107.003 11.959 106.213 12.9966 106.202C18.108 106.153 23.1622 105.912 28.1483 105.49C29.9475 55.9688 64.5938 16.4719 107.07 16.4719C149.527 16.4719 184.161 55.9324 185.99 105.421C191.253 105.887 196.593 106.15 201.998 106.203C203.04 106.213 203.898 107.01 203.991 108.027Z"
      fill="url(#paint0_linear)"
    />
    <path
      fillRule="evenodd"
      clipRule="evenodd"
      d="M189.393 107.845H189.393V107.547C189.393 107.646 189.393 107.746 189.393 107.845ZM189.389 106.899C188.942 55.9955 152.45 14.887 107.498 14.887C62.5512 14.887 26.0613 55.9875 25.6072 106.884C25.6782 105.844 26.5459 105.023 27.6039 105.011C31.8382 104.962 36.026 104.759 40.1586 104.409C41.6855 62.384 71.0875 28.8656 107.134 28.8656C143.165 28.8656 172.556 62.3531 174.108 104.351C178.477 104.737 182.908 104.96 187.392 105.011C188.455 105.023 189.326 105.852 189.389 106.899Z"
      fill="url(#paint1_linear)"
    />
    <g filter="url(#filter0_d)">
      <path
        d="M108 87.2424C74.4092 87.2453 41.2124 93.3463 8.53679 105.096C7.16499 105.589 6.01941 106.317 5.22096 107.204C4.42251 108.091 4.0008 109.104 4.00035 110.137C4.00035 147.413 3.75369 173.799 16.4417 196.696C29.1298 219.594 54.89 237.941 103.345 258.345C104.736 258.931 106.351 259.242 108 259.242C109.649 259.242 111.264 258.931 112.655 258.345C161.11 237.941 186.87 219.594 199.558 196.696C212.246 173.799 211.999 147.413 211.999 110.137C211.999 109.104 211.577 108.091 210.779 107.204C209.98 106.317 208.835 105.589 207.463 105.096C174.788 93.3468 141.593 87.2396 108 87.2424Z"
        fill="url(#paint2_radial)"
      />
    </g>
    <defs>
      <filter
        id="filter0_d"
        x="0"
        y="87.2424"
        width="216"
        height="180"
        filterUnits="userSpaceOnUse"
        colorInterpolationFilters="sRGB"
      >
        <feFlood floodOpacity="0" result="BackgroundImageFix" />
        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
        <feOffset dy="4" />
        <feGaussianBlur stdDeviation="2" />
        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0" />
        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
      </filter>
      <linearGradient
        id="paint0_linear"
        x1="108.023"
        y1="-11.4755"
        x2="108.023"
        y2="123.1"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#DFDFDF" />
        <stop offset="1" stopColor="#959595" />
        <stop offset="1" stopColor="#A2A2A2" />
      </linearGradient>
      <linearGradient
        id="paint1_linear"
        x1="156.532"
        y1="110.06"
        x2="64.2068"
        y2="9.38947"
        gradientUnits="userSpaceOnUse"
      >
        <stop stopColor="#C7C5C5" />
        <stop offset="1" stopColor="#959595" />
        <stop offset="1" stopColor="#5A5A5A" />
      </linearGradient>
      <radialGradient
        id="paint2_radial"
        cx="0"
        cy="0"
        r="1"
        gradientUnits="userSpaceOnUse"
        gradientTransform="translate(35 108) rotate(54.7618) scale(200.181 242.079)"
      >
        <stop stopColor="#E8E8E8" />
        <stop offset="0.552083" stopColor="#ACACAC" />
        <stop offset="1" stopColor="#636363" />
      </radialGradient>
    </defs>
  </g>
);

export default IconWrapper;

import React from 'react';
import { Box, Button, SvgIcon, makeStyles, Fade, useMediaQuery } from '@material-ui/core';
import LockOpenIcon from '@material-ui/icons/LockOpen';
import BluetoothIconSmall from '@material-ui/icons/Bluetooth';
import LockIcon from './LockVisualization';
import BluetoothIcon from '../img/Bluetooth.icon';
import { useAppContext } from '../context';
import { LockStatus, AppState } from '../types/app.types';
import { activateBluetooth } from '../actions/connection.actions';
import { openLock } from '../actions/lock.actions';
import StatusBox from '../components/StatusBox';

enum ComponentState {
  BLUETOOTH_OFF = 'BLUETOOTH_OFF',
  CONNECTING = 'CONNECTING',
  LOCKED = 'LOCKED',
  OPENING = 'OPENING',
  OPEN = 'OPEN',
}

const calculateComponentState = (state: AppState): ComponentState => {
  if (!state.connection.bluetoothOn) {
    return ComponentState.BLUETOOTH_OFF;
  }
  if (!state.connection.connected) {
    return ComponentState.CONNECTING;
  }
  switch (state.lock.status) {
    case LockStatus.LOCKED:
      return ComponentState.LOCKED;
    case LockStatus.OPENING:
      return ComponentState.OPENING;
    case LockStatus.OPEN:
      return ComponentState.OPEN;
    default:
      return ComponentState.CONNECTING;
  }
};

const useStyles = makeStyles(() => ({
  root: {
    overflow: 'hidden',
    display: 'flex',
    flex: 'none',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 40,
  },
  lockButton: {
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    borderRadius: '50%',
    padding: 20,
    width: 80,
    height: 80,
    // Align to physical center of lock
    position: 'absolute',
    top: 'calc(63%)',
    transform: 'translateY(-50%)',
  },
}));

const Lock: React.FC = () => {
  const { state, dispatch } = useAppContext();
  const styles = useStyles();
  const onActivate = () => activateBluetooth(dispatch);
  const onOpen = () => openLock(dispatch);

  const isSmall = useMediaQuery('(max-height:  400px)');
  const size = isSmall ? '230px' : '300px';

  return (
    <Fade in>
      <Box className={styles.root} height={size} p={2}>
        {
          {
            [ComponentState.BLUETOOTH_OFF]: (
              <>
                <StatusBox message="Bluetooth ist deaktiviert" />
                <BluetoothIcon />
                <Button color="primary" variant="contained" onClick={onActivate}>
                  <SvgIcon>
                    <BluetoothIconSmall />
                  </SvgIcon>
                  Aktivieren
                </Button>
              </>
            ),
            [ComponentState.CONNECTING]: (
              <>
                <StatusBox message="AirLock wird gesucht" />
                <Box position="relative" display="flex" justifyContent="center" alignItems="center">
                  <LockIcon variant="subtle" loading />
                </Box>
              </>
            ),
            [ComponentState.LOCKED]: (
              <>
                <StatusBox message="Geschlossen" />
                <Box position="relative" display="flex" justifyContent="center" alignItems="center" onClick={onOpen}>
                  <LockIcon variant="locked" />
                  <Button color="primary" variant="contained" onClick={onOpen} className={styles.lockButton}>
                    <Box flexDirection="column" alignItems="center">
                      <SvgIcon>
                        <LockOpenIcon />
                      </SvgIcon>
                      <Box mt={-1}>Öffnen</Box>
                    </Box>
                  </Button>
                </Box>
              </>
            ),
            [ComponentState.OPENING]: (
              <>
                <StatusBox message="Wird geöffnet..." />
                <Box position="relative" display="flex" justifyContent="center" alignItems="center">
                  <LockIcon variant="opening" loading />
                </Box>
              </>
            ),
            [ComponentState.OPEN]: (
              <>
                <StatusBox message="Offen" />
                <LockIcon variant="open" />
              </>
            ),
          }[calculateComponentState(state)]
        }
      </Box>
    </Fade>
  );
};

export default Lock;

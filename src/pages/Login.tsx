import React from 'react';
import { Box, Button, makeStyles, CircularProgress } from '@material-ui/core';
import { Formik, Form, Field } from 'formik';
import { TextField } from 'formik-material-ui';
import { useAppContext } from '../context';
import Logo from '../img/Logo.icon';
import { AuthAction } from '../reducers/auth.reducer';
import { tryLogin } from '../services/auth';

interface MyFormValues {
  email: string;
  password: string;
}

const useStyles = makeStyles(({ palette }) => ({
  root: {},
  logo: {
    width: 200,
    '--primary': palette.primary.main,
    '--secondary': palette.primary.contrastText,
  },
}));

const Login: React.FC = () => {
  const styles = useStyles();
  const { dispatch } = useAppContext();

  // mocked this to facilitate quick testing
  const initialValues: MyFormValues = { email: '', password: '' };

  return (
    <Box display="flex" flexDirection="column" justifyContent="center" alignItems="center">
      <Formik
        initialValues={initialValues}
        validate={(values) => {
          const errors: Partial<MyFormValues> = {};
          if (!values.email) {
            errors.email = 'Email-Adresse ist erforderlich';
          } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)) {
            errors.email = 'Ungültige Email-Adresse';
          }
          if (!values.password || values.password.length < 4) {
            errors.password = 'Mindestens 4 Zeichen erforderlich';
          }
          return errors;
        }}
        onSubmit={(values, { setSubmitting }) => {
          return tryLogin(values.email, values.password)
            .then(() => dispatch({ type: AuthAction.LOGIN, payload: { email: values.email } }))
            .catch((error) => {
              console.error(error);
              setSubmitting(false);
            });
        }}
      >
        {({ submitForm, isSubmitting, isValid, dirty }) => (
          <Form>
            <Box display="flex" flexDirection="column" alignItems="stretch">
              <Box mb={3} display="flex" justifyContent="center">
                <Logo className={styles.logo} />
              </Box>
              <Box height={80} display="flex" justifyContent="center">
                <Field component={TextField} name="email" type="email" label="Email" aria-label="email" fullWidth />
              </Box>
              <Box height={80} display="flex" justifyContent="center">
                <Field
                  component={TextField}
                  type="password"
                  label="Passwort"
                  name="password"
                  aria-label="password"
                  fullWidth
                />
              </Box>
              <Box display="flex" justifyContent="center" height={40}>
                {!isSubmitting ? (
                  <Button
                    type="submit"
                    variant="contained"
                    color="primary"
                    disabled={!dirty || isSubmitting || !isValid}
                    onClick={submitForm}
                    fullWidth
                  >
                    Anmelden
                  </Button>
                ) : (
                  <CircularProgress />
                )}
              </Box>
            </Box>
          </Form>
        )}
      </Formik>
    </Box>
  );
};

export default Login;

import React from 'react';
import { render, screen, userEvent, waitFor, act } from '../test/utils';

import Lock from './Lock';
import { AppState, LockStatus } from '../types/app.types';
import * as bluetooth from '../services/bluetooth';
import { getInitialMockState } from '../test/mocks';

const renderLock = (state: AppState) => render(<Lock />, { providerProps: { state } });

describe('Lock', () => {
  test('renders a description that bluetooth is off', () => {
    renderLock({
      ...getInitialMockState(),
      connection: {
        bluetoothOn: false,
        connected: false,
      },
      lock: {
        status: LockStatus.LOCKED,
      },
    });

    expect(screen.getByText(/Bluetooth ist deaktiviert/)).toBeInTheDocument();
  });
  test('shows that lock is being searched ', () => {
    renderLock({
      ...getInitialMockState(),
      connection: {
        bluetoothOn: true,
        connected: false,
      },
      lock: {
        status: LockStatus.LOCKED,
      },
    });

    expect(screen.getByText(/wird gesucht/)).toBeInTheDocument();
  });
  test('hides lock status when no connection is established ', () => {
    renderLock({
      ...getInitialMockState(),
      connection: {
        bluetoothOn: true,
        connected: false,
      },
      lock: {
        status: LockStatus.LOCKED,
      },
    });

    expect(screen.queryByText(/Geschlossen/)).toBeNull();
  });
  test('shows locked state when connected and locked', () => {
    renderLock({
      ...getInitialMockState(),
      connection: {
        bluetoothOn: true,
        connected: true,
      },
      lock: {
        status: LockStatus.LOCKED,
      },
    });

    expect(screen.getByText(/Geschlossen/)).toBeInTheDocument();
  });
  test('shows opening state when connected and opening', () => {
    renderLock({
      ...getInitialMockState(),
      connection: {
        bluetoothOn: true,
        connected: true,
      },
      lock: {
        status: LockStatus.OPENING,
      },
    });

    expect(screen.getByText(/Wird geöffnet/)).toBeInTheDocument();
  });
  test('shows open state when connected and open', () => {
    renderLock({
      ...getInitialMockState(),
      connection: {
        bluetoothOn: true,
        connected: true,
      },
      lock: {
        status: LockStatus.OPEN,
      },
    });

    expect(screen.getByText(/Offen/)).toBeInTheDocument();
  });
  test('Lock is searched after user activates bluetooth', async () => {
    const mockAsyncCall = (): Promise<void> => new Promise((resolve) => setTimeout(resolve, 0));
    jest.spyOn(bluetooth, 'tryActivateBluetooth').mockImplementation(mockAsyncCall);
    jest.spyOn(bluetooth, 'trySearchAndConnect').mockImplementation(mockAsyncCall);

    renderLock({
      ...getInitialMockState(),
      connection: {
        bluetoothOn: false,
        connected: false,
      },
      lock: {
        status: LockStatus.LOCKED,
      },
    });
    await act(async () => userEvent.click(screen.getByRole('button')));
    await waitFor(() => expect(screen.getByText(/Geschlossen/)).toBeInTheDocument());
    jest.restoreAllMocks();
  });
  test('Lock can be opened with a click', async () => {
    jest.spyOn(bluetooth, 'tryOpenLock').mockImplementation(() => new Promise((resolve) => setTimeout(resolve, 0)));

    renderLock({
      ...getInitialMockState(),
      connection: {
        bluetoothOn: true,
        connected: true,
      },
      lock: {
        status: LockStatus.LOCKED,
      },
    });
    await act(async () => userEvent.click(screen.getByRole('button')));
    await waitFor(() => expect(screen.getByText(/Offen/)).toBeInTheDocument());

    jest.restoreAllMocks();
  });
});

import React from 'react';
import { App } from './App';
import { render, screen } from './test/utils';
import { getInitialMockState } from './test/mocks';

describe('App', () => {
  test('renders App and Header component', () => {
    render(<App />, {
      providerProps: {
        state: {
          ...getInitialMockState(),
          auth: {
            loggedIn: true,
            email: 'mrfoo@gmail.com',
          },
        },
      },
    });

    expect(screen.getByRole('banner')).toBeInTheDocument();
  });
});

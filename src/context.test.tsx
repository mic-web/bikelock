import { render } from '@testing-library/react';
import React from 'react';
import { AppProvider, useAppContext, AppActionTypes } from './context';
import { AppState } from './types/app.types';

describe('AppContext', () => {
  test('Provider and Hook can be used to access state and dispatch actions', () => {
    let componentState: AppState | null = null;
    let componentDispatch: React.Dispatch<AppActionTypes> | null = null;
    // hook has to be used in component to test it
    const Consumer: React.FC = () => {
      const { state, dispatch } = useAppContext();
      componentState = state;
      componentDispatch = dispatch;
      return <div />;
    };
    render(
      <AppProvider>
        <Consumer />
      </AppProvider>
    );
    expect(componentState.connection).toBeDefined();
    expect(componentDispatch).toBeDefined();
  });
});

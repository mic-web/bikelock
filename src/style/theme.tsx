import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { CssBaseline } from '@material-ui/core';
import React from 'react';
import GlobalStyle from './GlobalStyle';

import woff from '../fonts/fira-sans-v10-latin-regular.woff';
import woff2 from '../fonts/fira-sans-v10-latin-regular.woff2';

const spacing = 8;
const fontName = 'Fira Sans';

const fontFace = {
  fontFamily: fontName,
  fontStyle: 'normal',
  fontWeight: 400,
  src: `
    url('${woff2}') format('wof=f2'), /* Super Modern Browsers */
    url('${woff}') format('woff'), /* Pretty Modern Browsers */
  `,
};

const theme = createMuiTheme({
  spacing,
  palette: {
    primary: {
      main: '#223344',
    },
    secondary: {
      main: '#37b977',
    },
    background: {
      default: '#f9f9f9',
      paper: '#e9e9e9',
    },
  },
  typography: {
    fontSize: 14,
    fontFamily: `'${fontName}', '-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Roboto', 'Oxygen','Ubuntu', 'Cantarell', 'Fira Sans', 'Droid Sans', 'Helvetica Neue'`,
  },
  overrides: {
    MuiButton: {
      root: {
        borderRadius: 50,
        textTransform: 'none',
      },
    },
    MuiCssBaseline: {
      '@global': {
        '@font-face': [fontFace],
      },
    },
  },
});

const AppThemeProvider: React.FC = ({ children }) => (
  <ThemeProvider theme={theme}>
    <GlobalStyle />
    <CssBaseline />
    {children}
  </ThemeProvider>
);

// eslint-disable-next-line import/prefer-default-export
export { AppThemeProvider };

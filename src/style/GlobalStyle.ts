import { withStyles } from '@material-ui/core';

const GlobalCss = withStyles({
  // @global is handled by jss-plugin-global.
  '@global': {
    body: {
      touchAction: 'manipulation', // allow zoom on mobile
    },
  },
})(() => null);

export default GlobalCss;

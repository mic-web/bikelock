# Summary

Prototypical WebApp for a wireless bike lock.

The prototype can be used in a loop:

1. Login
2. Connection status -> activate bluetooth
3. Connecting
4. Closed -> click open
5. Opening
6. Open -> click logout
7. Logout -> starts again at 1. (email is remembered)

Demo:
https://sad-mclean-fb2eb7.netlify.app/

## Installation

1. Clone the repository
2. `npm install`

## Usage

- Development server:
  `npm start`

- Run tests (with file watcher):
  `npm run test`

- Production build:
  `npm run build`
